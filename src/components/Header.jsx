import React from 'react';

function Header (){
    return(
        <>
        <h1 className='font-black text-5xl text-center w-3/3'>
            Seguimiento de Pacientes{" "}
            <span className='text-indigo-600'>Veterinaria</span>
        </h1>
        </>


    )
}

export default Header;