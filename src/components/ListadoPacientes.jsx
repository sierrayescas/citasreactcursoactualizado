
import Paciente from "./Paciente"
function ListadoPacientes({pacientes,setPaciente,eliminarPaciente}) {



  return (
    <div className=" md:w-1/2 lg:w3/5 md:h-screen overflow-scroll">
      {pacientes &&pacientes.length?(
        <>
            <h2 className="font-black text-3xl text-center">ListadoPacientes</h2>
            <p className=" text-xl mt-5 mb-10 text-center">
              Administra tus {''}
              <span className="text-indigo-600 font-bold">Pacientes y Citas</span>
            </p>
            </>
      ):(
          <>
          <h2 className="font-black text-3xl text-center">No hay pacientes que mostrar</h2>
            <p className=" text-xl mt-5 mb-10 text-center">
              Agrega pacientes para {''}
              <span className="text-indigo-600 font-bold">Comenzar</span>
            </p>
          </>


      )}

    {pacientes.map((paciente)=>{

      return(<Paciente
      key={paciente.id}
        paciente={paciente}
      setPaciente = {setPaciente}
      eliminarPaciente={eliminarPaciente}
      />);
    })}

    
    </div>
     
  )
}

export default ListadoPacientes